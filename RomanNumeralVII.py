def roman_numeral(num):

    if(num <= 3):
        return num * 'I'

    elif(num == 4):
        return 'IV'

    elif(num == 5):
        return 'V'

    elif(num == 6):
        return 'VI'

    return 'VII'

print(roman_numeral(1))
print(roman_numeral(2))
print(roman_numeral(3))
print(roman_numeral(3))
print(roman_numeral(4))
print(roman_numeral(5))
print(roman_numeral(6))
print(roman_numeral(7))
