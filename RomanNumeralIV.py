# Write the simplest code you can to make the function roman_numeral pass its tests.
# It now tests 4 -> "IV" in addition to all of the previous tests.


def roman_numeral(num):
    if num <= 3:
        return 'I' * num

    return 'IV'

print(roman_numeral(3))
print(roman_numeral(4))
