def make_integer(nums, length):
    nums = sorted(nums)
    value = 0
    for i in range(length):
        value = value * 10 + nums[i]
    return value
