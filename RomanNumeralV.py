# Write the simplest code you can to make the function roman_numeral pass its tests.
# It now tests 5 -> "V" in addition to all of the previous tests.

def roman_numeral(num):
    if (num <= 3):
        return "I" * num

    elif(num == 4):
        return 'IV'

    else:
        return 'V'


print(roman_numeral(3))
print(roman_numeral(4))
print(roman_numeral(5))
