def roman_numeral(num):

    if(num <= 3):
        return num * 'I'

    elif(num == 4):
        return 'IV'

    elif(num == 5):
        return 'V'

    return 'VI'

print(roman_numeral(3))
print(roman_numeral(4))
print(roman_numeral(5))
print(roman_numeral(6))
